# PXtoEM-CSS
- **[PXtoEM Website](http://pxtoem.com)**

| stylesheet | **->** | body font size |
|:-----------|:-------|:---------------|
| pxtoem6.css & pxtoem6.min.css   | **->** | PXtoEM.com CSS with font  6px (0.375em/5pts)  |
| pxtoem8.css & pxtoem8.min.css   | **->** | PXtoEM.com CSS with font  8px (0.500em/6pts)  |
| pxtoem10.css & pxtoem10.min.css | **->** | PXtoEM.com CSS with font 10px (0.625em/8pts)  |
| pxtoem12.css & pxtoem12.min.css | **->** | PXtoEM.com CSS with font 12px (0.750em/9pts)  |
| pxtoem14.css & pxtoem14.min.css | **->** | PXtoEM.com CSS with font 14px (0.875em/11pts) |
| pxtoem16.css & pxtoem16.min.css | **->** | PXtoEM.com CSS with font 16px (1.000em/12pts) |
| pxtoem18.css & pxtoem18.min.css | **->** | PXtoEM.com CSS with font 18px (1.125em/14pts) |
| pxtoem20.css & pxtoem20.min.css | **->** | PXtoEM.com CSS with font 20px (1.250em/15pts) |
| pxtoem22.css & pxtoem22.min.css | **->** | PXtoEM.com CSS with font 22px (1.375em/17pts) |
| pxtoem24.css & pxtoem24.min.css | **->** | PXtoEM.com CSS with font 24px (1.500em/18pts) |

# Tools
- **[CSS-Cleaner](https://html-cleaner.com/css)**
- **[CSS-Minifier](https://cssminifier.com)**
